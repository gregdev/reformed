<?php

class reformed_action_database_store
{
    public static function register()
    {
        add_action('reformed/actions/register', function() {
            reformed_actions::register('Database Store', 'reformed_action_database_store::store_submission', [
                [
                    'type' => 'text',
                    'name' => 'Submission Title',
                ]
            ]);
        });

        self::setup_admin_columns();
    }

    public static function store_submission(reformed_form $form, $data)
    {
        $title  = reformed_actions::get_option($form, 'Database Store', 'Submission Title');
        $fields = $data['reformed_field'];

        $title = preg_replace_callback('/({{(.*)}})/mU', function($matches) use ($fields) {
            if (isset($fields[$matches[2]])) {
                return $fields[$matches[2]];
            }
        }, $title);

        wp_insert_post([
            'post_title' => $title,
            'post_status' => 'publish',
            'post_type' => 'reformed_submission',
            'post_parent' => $form->ID,
            'meta_input' => [
                'reformed_form_data' => $data['reformed_field'],
                'submitted_post_id' => $data['post_id'],
            ],
        ]);
    }

    private static function setup_admin_columns() {
        add_filter('manage_reformed_form_posts_columns', function($columns) {
            unset($columns['date']);

            $columns['submissions'] = 'Submissions' ;
            $columns['date'] = 'Date' ;

            return $columns;
        });

        add_action('manage_reformed_form_posts_custom_column', function($column, $post_id) {
            global $wpdb; /** @var wpdb $wpdb */

            if ($column == 'submissions') {
                if (!get_post_meta($post_id, 'reformed-action/database-store/enabled', true)) {
                    echo 'Storage Disabled';
                } else {
                    $submission_count = $wpdb->get_var($wpdb->prepare("
                        SELECT  COUNT(*) AS count
                        FROM    {$wpdb->prefix}posts
                        WHERE   post_type   = 'reformed_submission'
                        AND     post_parent = %d
                        AND     post_status = 'publish'
                    ", $post_id));

                    $submission_url = admin_url("edit.php?post_type=reformed_submission&post_parent=$post_id");

                    echo "<a href='$submission_url'>";
                    echo $submission_count . ' submission';

                    if ($submission_count != 1) {
                        echo 's';
                    }

                    echo '</a>';
                }
            }
        }, 10, 2);
    }
}

reformed_action_database_store::register();
