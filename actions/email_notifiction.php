<?php

class reformed_action_email_notification
{
    public static function register()
    {
        add_action('reformed/actions/register', function() {
            reformed_actions::register('Email Notification', 'reformed_action_email_notification::send_email', [
                [
                    'type' => 'text',
                    'name' => 'Email Address',
                ],
                [
                    'type' => 'text',
                    'name' => 'Subject',
                ]
            ]);
        });
    }

    public static function send_email(reformed_form $form, $data)
    {
        $lines = [];

        foreach ($data['reformed_field'] as $field_name => $value) {
            if (is_array($value)) {
                $value = implode(', ', $value);
            }

            $lines []= "<p>$field_name: <strong>$value</strong></p>";
        }

        wp_mail(
            reformed_actions::get_option($form, 'Email Notification', 'Email Address'   ),
            reformed_actions::get_option($form, 'Email Notification', 'Subject'         ),
            implode("\r\n", $lines),
            [ 'Content-Type: text/html; charset=UTF-8' ]
        );
    }
}

reformed_action_email_notification::register();
