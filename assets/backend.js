'use strict'

;(function ($) {
    var $add_new_button = $('a[href$="post-new.php?post_type=reformed_submission"]')

    $add_new_button.after(
        '<a href="' +
            window.location.href +
            '&reformed_export=1" class="button-primary">Export</a>',
    )
    $add_new_button.remove()
})(jQuery)
