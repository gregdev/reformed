;(function ($) {
    window.reformedInit = function () {
        $('.reformed-form.natural-language').each(function () {
            const $form = $(this)

            $form.find('.reformed-field--text').each(function () {
                const $field = $(this)
                const $label = $field.find('> span:first-child')

                $label.attr('contenteditable', true)
                $label.attr('data-placeholder', $field.text())
                $label.text('')
            })
        })

        const selector = '.reformed-field input, .reformed-field textarea'

        $(selector).each(function () {
            const $field = $(this)

            $field.on('invalid', () => {
                $field.parents('.reformed-field').addClass('is--invalid')
            })
        })

        $('.reformed-form.natural-language [contenteditable]').each(function () {
            const $field = $(this)

            $field.css('min-width', $field.width()).addClass('absoluted')
        })
    }
})(jQuery)

reformedInit()
;(function ($) {
    // Field labels that get out of your way
    const $body = $('body')
    const selector = '.reformed-field input, .reformed-field textarea'

    $body.on('focus', selector, function () {
        $(this).parents('.reformed-field').removeClass('is--dormant')
    })

    $body.on('blur change', selector, function () {
        $(this)
            .parents('.reformed-field')
            .toggleClass('is--dormant', !this.value && !this.placeholder)
    })

    $(selector).trigger('blur')
})(jQuery)

// natural language inputs
;(function ($) {
    const $body = $('body')

    $body.on('input', '.reformed-form.natural-language [contenteditable]', function () {
        const editable = this
        const $editable = $(editable)

        $editable.next().find('input').val($editable.text())
    })
})(jQuery)
;(function ($) {
    $('body').on('submit', 'form.reformed-form', function (e) {
        e.preventDefault()

        const $form = $(this)
        const options = reformed.forms[$form.data('form-id')]

        $form.trigger('reformedSubmitted')

        if ($form.hasClass('is--loading')) {
            return
        }

        $form
            .addClass('is--loading')
            .removeClass('is--success')
            .removeClass('has--error')
            .find('.reformed--messages')
            .text('')

        $.ajax({
            url: reformed.ajax_url,
            type: 'post',
            data: `${$form.serialize()}&action=reformed_submit`,
        })
            .always(() => {
                $form.removeClass('is--loading')
            })
            .fail(() => {
                $form
                    .addClass('has--error')
                    .trigger('reformedSubmitFail')
                    .find('.reformed--messages')
                    .text(options.messages.failure)
            })
            .done(() => {
                $form.addClass('is--success')

                if (options.clear_on_success) {
                    $form
                        .trigger('reset')
                        .find('input textarea')
                        .trigger('blur')
                        .end()
                        .find('[contenteditable]')
                        .text('')
                }

                $form
                    .trigger('reformedSubmitSuccess')
                    .find('.reformed--messages')
                    .text(options.messages.success)
            })
    })
})(jQuery)
