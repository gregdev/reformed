<?php

global $post;

if (!$attrs['form']) {
    return;
}

$form = reformed::get_form($attrs['form']);

?>

<form class="reformed-form reformed-form--<?= $form->ID ?> <?= $form->is_natural_language() ? 'natural-language' : '' ?>" data-form-id="<?= $form->ID ?>" method=post>

    <?php do_action('reformed/fields/before', $form) ?>

    <?php wp_nonce_field('action-reformed-submit') ?>

    <div class=reformed--fields>
        <?= apply_filters('the_content', $form->post_content) ?>
    </div>

    <?php do_action('reformed/fields/after', $form) ?>

    <div class=reformed--messages></div>

    <?php do_action('reformed/messages/after', $form) ?>

    <input type=hidden name=form_id value="<?= esc_attr($form->ID) ?>">

    <?php if (isset($post)) : ?>
        <input type=hidden name=post_id value="<?= esc_attr($post->ID) ?>">
    <?php endif ?>

</form>
