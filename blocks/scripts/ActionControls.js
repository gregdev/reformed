class ReformedActionControls extends wp.element.Component {
    constructor() {
        super(...arguments)
        this.action = arguments[0].action

        this.state = wp.data.select('core/editor').getEditedPostAttribute('meta')

        this.editPostMeta = this.editPostMeta.bind(this)
    }

    editPostMeta(meta) {
        const { editPost } = wp.data.dispatch('core/editor')
        this.setState(meta)
        editPost({ meta: meta })
    }

    render() {
        var that = this
        var controls = []

        controls.push(
            wp.element.createElement(wp.components.ToggleControl, {
                key: `reformed-action/${that.action.code}`,
                label: 'Enabled',
                checked: this.state['reformed-action/' + that.action.code + '/enabled'],
                onChange: function (value) {
                    that.editPostMeta({
                        ['reformed-action/' + that.action.code + '/enabled']: value,
                    })
                },
            }),
        )

        if (this.state['reformed-action/' + that.action.code + '/enabled']) {
            this.action.fields.forEach(function (field) {
                var control = wp.components.TextControl
                var options = []

                if (field.interface == 'toggle') {
                    control = wp.components.ToggleControl
                } else if (field.interface == 'select') {
                    control = wp.components.SelectControl

                    if (field.source == 'field_blocks') {
                        options.push({ label: '', value: '' })

                        wp.data
                            .select('core/block-editor')
                            .getBlocks()
                            .forEach(function (block) {
                                if (block.name == 'reformed/field-text') {
                                    options.push({
                                        label: block.attributes.label,
                                        value: block.attributes.label,
                                    })
                                }
                            })
                    }
                }

                controls.push(
                    el(control, {
                        key: `reformed-action/${that.action.code}/${field.code}`,
                        label: field.name,
                        value: that.state['reformed-action/' + that.action.code + '/' + field.code],
                        onChange: function (value) {
                            that.editPostMeta({
                                ['reformed-action/' + that.action.code + '/' + field.code]: value,
                            })
                        },
                        options: options,
                    }),
                )
            })
        }

        return controls
    }
}
