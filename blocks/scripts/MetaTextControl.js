;(function () {
    const el = wp.element.createElement
    const { TextControl } = wp.components
    const { withDispatch, withSelect } = wp.data

    window['ReformedMetaTextControl'] = wp.compose.compose(
        withDispatch(function (dispatch, props) {
            return {
                setMetaValue: function (metaValue) {
                    dispatch('core/editor').editPost({ meta: { [props.metaKey]: metaValue } })
                },
            }
        }),
        withSelect(function (select, props) {
            return {
                metaValue: select('core/editor').getEditedPostAttribute('meta')[props.metaKey],
            }
        }),
    )(function (props) {
        if (typeof props.metaValue == 'undefined') {
            props.metaValue = props.default
        }

        return el(TextControl, {
            label: props.title,
            value: props.metaValue,
            onChange: function (value) {
                props.setMetaValue(value)
            },
        })
    })
})()
