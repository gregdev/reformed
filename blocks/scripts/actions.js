;(function (wp) {
    const el = wp.element.createElement

    reformed_actions.forEach(function (action) {
        wp.plugins.registerPlugin('reformed--action--' + action.code, {
            name: 'reformed--' + action.name,
            category: 'common',
            icon: action.icon,

            render: function (props) {
                if (wp.data.select('core/editor').getCurrentPostType() != 'reformed_form') {
                    return null
                }

                return el(
                    wp.editPost.PluginDocumentSettingPanel,
                    {
                        className: 'reformed--action--' + action.code,
                        title: action.name,
                    },
                    [
                        el(ReformedActionControls, {
                            action: action,
                        }),
                    ],
                )
            },
        })
    })
})(window.wp)
