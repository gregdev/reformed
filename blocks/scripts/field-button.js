;(function () {
    const el = wp.element.createElement
    const { registerBlockType } = wp.blocks
    const { RichText } = wp.blockEditor

    if (
        wp.data
            .select('core/blocks')
            .getCategories()
            .find((cat) => cat.slug == 'reformed-fields') == null
    ) {
        return
    }

    registerBlockType('reformed/button', {
        title: 'Submit Button',
        icon: 'editor-removeformatting',
        category: 'reformed-fields',

        attributes: {
            label: {
                default: '',
                type: 'string',
            },
        },

        edit: function (props) {
            return el(
                'button',
                {
                    type: 'submit',
                },
                el(
                    'span',
                    {},
                    el(RichText, {
                        value: props.attributes.label,
                        placeholder: 'Button Label...',
                        onChange: function (value) {
                            props.setAttributes({ label: value })
                        },
                    }),
                ),
            )
        },

        save: function (props) {
            return el(
                'div',
                { className: 'reformed--button-wrap' },
                el('button', { type: 'submit' }, el('span', {}, props.attributes.label)),
            )
        },
    })
})()
