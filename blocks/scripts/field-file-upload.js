;(function () {
    const el = wp.element.createElement
    const { registerBlockType } = wp.blocks
    const { InspectorControls, RichText } = wp.blockEditor
    const { SelectControl, PanelBody, ToggleControl } = wp.components

    if (
        wp.data
            .select('core/blocks')
            .getCategories()
            .find((cat) => cat.slug == 'reformed-fields') == null
    ) {
        return
    }

    registerBlockType('reformed/field-file-upload', {
        title: 'File Upload',
        icon: 'upload',
        category: 'reformed-fields',

        attributes: {
            label: {
                default: '',
                type: 'string',
            },
            required: {
                default: false,
                type: 'boolean',
            },
        },

        edit: function (props) {
            var classRequired = props.attributes.required ? 'is--required' : ''

            return [
                el(
                    'div',
                    {
                        className: 'reformed-field reformed-field--file-upload ' + classRequired,
                        attributes: props.attributes,
                    },
                    el(
                        'span',
                        {},
                        el(RichText, {
                            value: props.attributes.label,
                            placeholder: 'Field Label...',
                            onChange: function (value) {
                                props.setAttributes({ label: value })
                            },
                        }),
                    ),
                    el('input', {
                        type: 'file',
                        disabled: true,
                    }),
                ),
                el(
                    InspectorControls,
                    {},
                    el(
                        PanelBody,
                        {},
                        el(ToggleControl, {
                            key: 'required',
                            label: 'Required',
                            checked: props.attributes.required,
                            onChange: function (state) {
                                props.setAttributes({ required: state })
                            },
                        }),
                    ),
                ),
            ]
        },

        save: function (props) {
            var classRequired = props.attributes.required ? 'is--required' : ''

            return el(
                'label',
                {
                    className: 'reformed-field reformed-field--file-upload ' + classRequired,
                    attributes: props.attributes,
                },
                el('span', {}, props.attributes.label),
                el('input', {
                    type: 'file',
                    name: 'reformed_field[' + props.attributes.label + ']',
                    required: props.attributes.required,
                }),
            )
        },
    })
})()
