;(function () {
    const el = wp.element.createElement
    const { registerBlockType } = wp.blocks
    const { InspectorControls, RichText } = wp.blockEditor
    const { SelectControl, PanelBody, ToggleControl, TextareaControl } = wp.components

    if (
        wp.data
            .select('core/blocks')
            .getCategories()
            .find((cat) => cat.slug == 'reformed-fields') == null
    ) {
        return
    }

    function getInputs(props) {
        var inputs = []

        if (props.attributes.type == 'select') {
            var options = []

            props.attributes.options.split('\n').forEach(function (option) {
                var label = option
                var value = option

                if (option.indexOf(' : ') !== -1) {
                    var pieces = option.split(' : ')
                    value = pieces[0]
                    label = pieces[1]
                }

                options.push('<option value="' + value + '">' + label + '</option>')
            })

            var required = props.attributes.required ? 'required' : ''

            inputs.push(
                el('div', {
                    dangerouslySetInnerHTML: {
                        __html:
                            '<select name="reformed_field[' +
                            props.attributes.label +
                            ']" ' +
                            required +
                            '><options></options>' +
                            options.join('') +
                            '</select>',
                    },
                }),
            )
        } else if (props.attributes.type == 'radio' || props.attributes.type == 'checkbox') {
            var required =
                props.attributes.type == 'radio' && props.attributes.required ? 'required' : ''

            props.attributes.options.split('\n').forEach(function (option) {
                var label = option
                var value = option

                if (option.indexOf(' : ') !== -1) {
                    var pieces = option.split(' : ')
                    value = pieces[0]
                    label = pieces[1]
                }

                inputs.push(
                    el('label', {
                        dangerouslySetInnerHTML: {
                            __html:
                                '<input type=' +
                                props.attributes.type +
                                ' name="reformed_field[' +
                                props.attributes.label +
                                '][]" value="' +
                                value +
                                '" ' +
                                required +
                                '> <span>' +
                                label +
                                '</span>',
                        },
                    }),
                )
            })
        }

        return inputs
    }

    registerBlockType('reformed/field-options', {
        title: 'Options Fields',
        icon: 'edit',
        category: 'reformed-fields',

        attributes: {
            type: {
                default: 'select',
                type: 'string',
            },
            label: {
                default: '',
                type: 'string',
            },
            required: {
                default: false,
                type: 'boolean',
            },
            options: {
                default: '',
                type: 'string',
            },
        },

        edit: function (props) {
            var classRequired = props.attributes.required ? 'is--required' : ''

            var conditionalFields = []

            function getConditionalFields(blocks) {
                blocks.forEach(function (block) {
                    if (block.name == 'reformed/field-options') {
                        conditionalFields.push({
                            label: block.attributes.label,
                            options: block.attributes.options.split('\n'),
                        })
                    }

                    if (block.innerBlocks) {
                        getConditionalFields(block.innerBlocks)
                    }
                })
            }

            getConditionalFields(wp.data.select('core/editor').getBlocks())

            return [
                el(
                    'div',
                    {
                        className:
                            'reformed-field reformed-field--text type--' +
                            props.attributes.type +
                            ' ' +
                            classRequired,
                        attributes: props.attributes,
                    },
                    el(
                        'span',
                        {},
                        el(RichText, {
                            value: props.attributes.label,
                            placeholder: 'Field Label...',
                            onChange: function (value) {
                                props.setAttributes({ label: value })
                            },
                        }),
                    ),
                    ...getInputs(props),
                ),
                el(
                    InspectorControls,
                    {},
                    el(
                        PanelBody,
                        {},
                        el(SelectControl, {
                            key: 'type',
                            label: 'Type',
                            value: props.attributes.value,
                            options: [
                                { label: 'Select Box', value: 'select' },
                                { label: 'Radio Buttons', value: 'radio' },
                                { label: 'Check Boxes', value: 'checkbox' },
                            ],
                            onChange: function (value) {
                                props.setAttributes({ type: value })
                            },
                        }),
                        el(ToggleControl, {
                            key: 'required',
                            label: 'Required',
                            checked: props.attributes.required,
                            onChange: function (state) {
                                props.setAttributes({ required: state })
                            },
                        }),
                        el(TextareaControl, {
                            key: 'options',
                            label: 'Options',
                            help: 'One option per line, use a colon to separate value from label, eg. red : Red',
                            value: props.attributes.options,
                            onChange: function (value) {
                                props.setAttributes({ options: value })
                            },
                        }),
                    ),
                ),
                /*el(wp.blockEditor.InspectorControls, {},
                    el(wp.components.PanelBody, { title: 'Conditional Logic' },
                        el(ToggleControl, {
                            key     : 'conditional_logic',
                            label   : 'Use Conditional Logic',
                            checked : props.attributes.conditional_logic,
                            onChange: function(state) {
                                props.setAttributes({ conditional_logic: state });
                            }
                        }),
                        el('div', {
                            dangerouslySetInnerHTML: {
                                __html: 'Hide this field if'
                            }
                        })
                    )
                )*/
            ]
        },

        save: function (props) {
            var classRequired = props.attributes.required ? 'is--required' : ''
            var tag = props.attributes.type == 'select' ? 'label' : 'div'

            return el(
                tag,
                {
                    className:
                        'reformed-field reformed-field--text type--' +
                        props.attributes.type +
                        ' ' +
                        classRequired,
                    attributes: props.attributes,
                },
                el('span', {}, props.attributes.label),
                ...getInputs(props),
            )
        },
    })
})()
