;(function () {
    const el = wp.element.createElement
    const { registerBlockType } = wp.blocks
    const { InspectorControls, RichText } = wp.blockEditor
    const { SelectControl, PanelBody, ToggleControl, TextControl } = wp.components

    if (
        wp.data
            .select('core/blocks')
            .getCategories()
            .find((cat) => cat.slug == 'reformed-fields') == null
    ) {
        return
    }

    function renderField(props, edit) {
        var prefix, suffix, label, info

        if (props.attributes.prefix) {
            prefix = el('span', { className: 'reformed--prefix' }, props.attributes.prefix)
        }

        if (props.attributes.suffix) {
            suffix = el('span', { className: 'reformed--suffix' }, props.attributes.suffix)
        }

        if (props.attributes.info) {
            suffix = el('span', { className: 'reformed--info' }, props.attributes.info)
        }

        var classRequired = props.attributes.required ? 'is--required' : ''

        if (!edit) {
            label = el('span', {}, props.attributes.label)
        } else {
            label = el(
                'span',
                {},
                el(RichText, {
                    value: props.attributes.label,
                    placeholder: 'Field Label...',
                    onChange: function (value) {
                        props.setAttributes({ label: value })
                    },
                }),
            )
        }

        var fieldName = props.attributes.label || props.attributes.placeholder

        return el(
            'label',
            {
                className:
                    'reformed-field reformed-field--text type--' +
                    props.attributes.type +
                    ' ' +
                    classRequired,
                attributes: props.attributes,
            },
            label,
            info,
            el(
                'span',
                { className: 'reformed--field-wrap' },
                prefix,
                el('input', {
                    type: props.attributes.type,
                    disabled: edit,
                    name: 'reformed_field[' + fieldName + ']',
                    required: props.attributes.required,
                    placeholder: props.attributes.placeholder,
                }),
                suffix,
            ),
        )
    }

    registerBlockType('reformed/field-text', {
        title: 'Text Field (single line)',
        icon: 'edit',
        category: 'reformed-fields',

        attributes: {
            type: {
                default: 'text',
                type: 'string',
            },
            label: {
                default: '',
                type: 'string',
            },
            placeholder: {
                default: '',
                type: 'string',
            },
            required: {
                default: false,
                type: 'boolean',
            },
            info: {
                default: '',
                type: 'string',
            },
            prefix: {
                default: '',
                type: 'string',
            },
            suffix: {
                default: '',
                type: 'string',
            },
        },

        edit: function (props) {
            return [
                renderField(props, true),
                el(
                    InspectorControls,
                    {},
                    el(
                        PanelBody,
                        {},
                        el(SelectControl, {
                            key: 'type',
                            label: 'Type',
                            value: props.attributes.value,
                            options: [
                                { label: 'Text', value: 'text' },
                                { label: 'Email Address', value: 'email' },
                                { label: 'Phone', value: 'tel' },
                                { label: 'Number', value: 'number' },
                                { label: 'Date', value: 'date' },
                            ],
                            onChange: function (value) {
                                props.setAttributes({ type: value })
                            },
                        }),
                        el(TextControl, {
                            key: 'placeholder',
                            label: 'Placeholder',
                            value: props.attributes.placeholder,
                            onChange: function (value) {
                                props.setAttributes({ placeholder: value })
                            },
                        }),
                        el(ToggleControl, {
                            key: 'required',
                            label: 'Required',
                            checked: props.attributes.required,
                            onChange: function (state) {
                                props.setAttributes({ required: state })
                            },
                        }),
                        el(TextControl, {
                            key: 'info',
                            label: 'Info',
                            value: props.attributes.info,
                            onChange: function (value) {
                                props.setAttributes({ info: value })
                            },
                        }),
                        el(TextControl, {
                            key: 'prefix',
                            label: 'Prefix',
                            value: props.attributes.prefix,
                            onChange: function (value) {
                                props.setAttributes({ prefix: value })
                            },
                        }),
                        el(TextControl, {
                            key: 'suffix',
                            label: 'Suffix',
                            value: props.attributes.suffix,
                            onChange: function (value) {
                                props.setAttributes({ suffix: value })
                            },
                        }),
                    ),
                ),
            ]
        },

        save: function (props) {
            return renderField(props, false)
        },
    })
})()
