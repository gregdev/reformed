;(function () {
    const el = wp.element.createElement
    const { InspectorControls } = wp.editor
    const { registerBlockType } = wp.blocks
    const { Placeholder, SelectControl, PanelBody } = wp.components

    var settings = {
        select_default: '',
        placeholder_text: 'Reformed Form',
        icon: 'carrot',
        settings_title: 'Form Options',
        edit_form: 'edit',
    }

    registerBlockType('reformed/form', {
        title: 'Reformed Form',
        icon: 'carrot',
        category: 'widgets',
        attributes: {
            form: {
                default: 0,
                type: 'integer',
            },
        },

        edit: function (props) {
            var formSelect = el(SelectControl, {
                value: props.attributes.form,
                options: reformed_forms,
                onChange: function (value) {
                    props.setAttributes({ form: parseInt(value) })
                },
            })

            if (!props.attributes.form) {
                return el(
                    Placeholder,
                    {
                        icon: settings.icon,
                        label: 'Select a Form',
                    },
                    formSelect,
                )
            } else {
                return [
                    el(Placeholder, {
                        icon: settings.icon,
                        label: settings.placeholder_text,
                    }),
                    el(
                        InspectorControls,
                        {},
                        el(PanelBody, { title: 'Reformed Form' }, formSelect),
                    ),
                ]
            }
        },

        save: function (props) {
            return null
        },
    })

    wp.plugins.registerPlugin('reformed--form-options', {
        name: 'reformed--form-options',
        category: 'common',
        icon: '',

        render: function (props) {
            if (wp.data.select('core/editor').getCurrentPostType() != 'reformed_form') {
                return null
            }

            var controls = []

            var options = [
                {
                    code: 'success-message',
                    label: 'Success Message',
                    type: 'string',
                    default: 'Form submitted successfully!',
                },
                {
                    code: 'failure-message',
                    label: 'Failure Message',
                    type: 'string',
                    default: 'Error submitting form',
                },
                {
                    code: 'clear-on-success',
                    label: 'Clear on Success',
                    type: 'bool',
                    default: true,
                },
                {
                    code: 'natural-language',
                    label: 'Natural Language',
                    type: 'bool',
                    default: false,
                },
            ]

            options.forEach(function (option) {
                var options = {
                    metaKey: 'reformed-form/options/' + option.code,
                    title: option.label,
                    default: option.default,
                }

                if (option.type == 'bool') {
                    controls.push(el(ReformedMetaToggleControl, options))
                } else {
                    controls.push(el(ReformedMetaTextControl, options))
                }
            })

            return el(
                wp.editPost.PluginDocumentSettingPanel,
                {
                    className: 'reformed--form-options',
                    title: 'Form Options',
                },
                ...controls,
            )
        },
    })
})()
