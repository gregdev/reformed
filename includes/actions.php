<?php

add_action('init', 'reformed_actions::init', 25);

class reformed_actions {

    protected static $actions;

    public static function init() {
        self::$actions = [];

        foreach (glob(__DIR__ . '/../actions/*.php') as $filename) {
            include $filename;
        }

        do_action('reformed/actions/register');
        self::$actions = apply_filters('reformed/form/actions', self::$actions);

        add_action('enqueue_block_editor_assets', function() {
            wp_enqueue_script(
                'reformed-actions',
                plugins_url('/blocks/scripts/actions.js', __DIR__),
                [ 'wp-plugins', 'wp-element', 'wp-components', 'wp-edit-post', 'wp-data', 'reformed-action-controls' ]
            );

            wp_localize_script('reformed-actions', 'reformed_actions', reformed_actions::get_actions());
        });
    }

    public static function register($name, $function, $fields = []) {
        foreach ($fields as &$field) {
            $field['code'] = sanitize_title($field['name']);
        }

        self::$actions []= [
            'code'      => sanitize_title($name),
            'name'      => $name                ,
            'function'  => $function            ,
            'fields'    => $fields              ,
        ];
    }

    public static function get_actions() {
        return self::$actions;
    }

    public static function get_option($form, $action_name, $option_name) {
        if (is_object($form)) {
            $form_id = $form->ID;
        } else {
            $form_id = $form;
        }

        $action_code = sanitize_title($action_name);
        $option_code = sanitize_title($option_name);

        return get_post_meta($form_id, "reformed-action/$action_code/$option_code", true);
    }

    public static function get_options($form, $action_name) {
        if (is_object($form)) {
            $form_id = $form->ID;
        } else {
            $form_id = $form;
        }

        $action_code    = sanitize_title($action_name);
        $option_prefix  = "reformed-action/{$action_code}/";

        $options = [];

        foreach (get_post_meta($form_id) as $key => $value) {
            if (strpos($key, $option_prefix) === 0) {
                $key = str_replace($option_prefix, '', $key);

                $options[$key] = $value[0];
            }
        }

        return $options;
    }

}