<?php

add_action('init', 'reformed_ajax::init');

class reformed_ajax {

    public static function init() {
        $actions = [ 'submit' ];

        foreach ($actions as $action) {
            add_action("wp_ajax_reformed_{$action}"          , "reformed_ajax::action_{$action}");
            add_action("wp_ajax_nopriv_reformed_{$action}"   , "reformed_ajax::action_{$action}");
        }
    }

    public static function action_submit() {
        if (!check_ajax_referer('action-reformed-submit', false, false)) {
            status_header(400);

            wp_send_json([
                'success' => false,
                'message' => 'Invalid nonce',
            ]);
        }

        $form = new reformed_form(get_post($_POST['form_id']));
        $form->submit($_POST);
    }

}