<?php

add_action('init', 'reformed_blocks::init', 20);

class reformed_blocks
{
    public static function init()
    {
        add_filter('block_categories_all', function($categories, $context) {
            if ($context->post->post_type == 'reformed_form') {
                $categories []= [
                    'slug' => 'reformed-fields',
                    'title' => 'Reformed Fields',
                ];
            }

            return $categories;
        }, 10, 2);

        $fields = [
            'text' => [
                'attributes' => [
                    'label' => [ 'type' => 'string' , 'default' => '' ],
                    'type' => [ 'type' => 'string' , 'default' => 'text'   ],
                    'required' => [ 'type' => 'boolean', 'default' => false     ],
                ]
            ],
            'textarea'  => [
                'attributes' => [
                    'label' => [ 'type' => 'string' , 'default' => '' ],
                    'required' => [ 'type' => 'boolean', 'default' => false ],
                ]
            ],
            'button'    => [
                'attributes' => [
                    'label' => [ 'type' => 'string' , 'default' => 'Submit' ],
                ]
            ],
            'options' => [
                'attributes' => [
                    'type' => [ 'type' => 'string' , 'default' => 'select' ],
                    'label' => [ 'type' => 'string' , 'default' => '' ],
                    'required' => [ 'type' => 'boolean', 'default' => false ],
                    'options' => [ 'type' => 'string' , 'default' => '' ],
                ]
            ],
            'file-upload' => [
                ' '    => [
                    'label' => [ 'type' => 'string', 'default' => '' ],
                    'required' => [ 'type' => 'boolean', 'default' => false ],
                ],
            ]
        ];

        $fields = apply_filters('reformed/fields', $fields);

        wp_register_script('reformed-form', plugins_url("../blocks/scripts/form.js", __FILE__), [ 'wp-blocks', 'wp-element', 'wp-editor', 'wp-block-editor', 'reformed-meta-toggle-control', 'reformed-meta-text-control' ]);

        register_block_type('reformed/form', [
            'editor_script' => 'reformed-form',
            'attributes' => [
                'block_type' => [ 'type' => 'string', 'default' => 'reformed-form' ],
                'form' => [ 'type' => 'integer', 'default' => 0 ],
            ],
            'render_callback' => 'reformed_blocks::render',
        ]);

        $forms = [ [ 'value' => 0, 'label' => '' ] ];

        foreach (reformed::get_forms() as $form) {
            $forms []= [
                'label' => $form->get_title(),
                'value' => $form->ID,
            ];
        }

        wp_localize_script('reformed-form', 'reformed_forms', $forms);

        foreach ($fields as $name => $block) {
            wp_register_script(
                "reformed-field-{$name}",
                plugins_url("../blocks/scripts/field-{$name}.js", __FILE__),
                [ 'wp-blocks', 'wp-element', 'wp-editor' ]
            );

            if (empty($block['attributes'])) {
                $block['attributes'] = [];
            }

            $args = [
                'editor_script' => "reformed-field-$name",
                'attributes' => $block['attributes'] + [ 'block_type' => [
                    'type' => 'string',
                    'default' => $name
                ]],
            ];

            if (!empty($block['server_side'])) {
                $args['render_callback'] = 'reformed_blocks::render';
            }

            register_block_type("reformed/field-$name", $args);

            if (isset($block['data'])) {
                wp_localize_script("reformed-field-$name", 'reformed_' . str_replace('-', '_', $name), $block['data']);
            }
        }
    }

    public static function render($attrs)
    {
        ob_start();

        if ($attrs['block_type'] == 'reformed-form') {
            include __DIR__ . "/../blocks/form.php";
        } else {
            include __DIR__ . "/../blocks/fields/{$attrs['block_type']}.php";
        }

        return ob_get_clean();
    }

}
