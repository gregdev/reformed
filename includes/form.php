<?php

class reformed_form {

    public function __construct($data) {
        if (is_object($data)) {
            foreach (get_object_vars($data) as $k => $v) {
                $this->{$k} = $v;
            }
        }
    }

    public function get_title() {
        return $this->post_title;
    }

    public function get_enabled_actions() {
        $enabled_actions = [];

        foreach (reformed_actions::get_actions() as $action) {
            if (get_post_meta($this->ID, "reformed-action/{$action['code']}/enabled", true)) {
                $enabled_actions []= $action;
            }
        }

        return $enabled_actions;
    }

    public function submit($data) {
        $form = reformed::get_form($data['form_id']);
        $data = apply_filters('reformed/submit' , $data, $this);

        do_action('reformed/submitted'          , $data, $this);
        do_action('reformed/run-actions/before' , $data, $this);

        foreach ($form->get_enabled_actions() as $action) {
            call_user_func($action['function'], $this, $data);
        }

        do_action('reformed/run-actions/after'  , $data, $this);
    }

    public function is_natural_language() : bool {
        return (bool) get_post_meta($this->ID, 'reformed-form/options/natural-language', true);
    }

}