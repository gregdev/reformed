<?php

add_action('init', 'reformed_post_type_form::init', 30);

class reformed_post_type_form
{
    public static function init()
    {
        self::register_post_type();
        self::setup_meta_fields();
    }

    private static function register_post_type()
    {
        register_post_type('reformed_form', [
            'label' => 'Forms',
            'public' => false,
            'show_ui' => true,
            'menu_icon' => 'dashicons-carrot',
            'supports' => [ 'editor', 'title', 'custom-fields' ],
            'show_in_rest' => true,
            //'capability_type' => 'reformed_forms',
            'labels' => [
                'singular_name' => 'Form',
                'add_new_item' => 'Add New Form',
                'edit_item' => 'Edit Form',
                'new_item' => 'New Form',
                'view_item' => 'View Form',
                'view_items' => 'View Forms',
                'search_itons' => 'Search Forms',
                'not_found' => 'No forms found',
                'not_found_in_trash' => 'No forms found in Trash',
                'all_items' => 'All Forms',
                'item_published' => 'Form published',
            ],
        ]);
    }

    private static function setup_meta_fields()
    {
        $form_options = [
            'Success Message' => 'string' ,
            'Failure Message' => 'string' ,
            'Clear on Success' => 'boolean',
            'Natural Language' => 'boolean',
        ];

        foreach ($form_options as $option => $type) {
            $code = 'reformed-form/options/' . sanitize_title($option);
            register_post_meta('reformed_form', $code, [
                'show_in_rest' => true ,
                'single' => true ,
                'type' => $type,
            ]);
        }

        foreach (reformed_actions::get_actions() as $action) {
            register_post_meta('reformed_form', "reformed-action/{$action['code']}/enabled", [
                'show_in_rest' => true,
                'single' => true,
                'type' => 'boolean',
            ]);

            foreach ($action['fields'] as $field) {
                register_post_meta('reformed_form', "reformed-action/{$action['code']}/{$field['code']}", [
                    'show_in_rest' => true,
                    'single' => true,
                    'type' => 'string',
                ]);
            }
        }
    }
}
