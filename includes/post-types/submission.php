<?php

add_action('init', 'reformed_post_type_submission::init', 40);

class reformed_post_type_submission {

    public static function init() {
        self::register_post_type();
        self::add_meta_box();

        // don't show the post type as a top-level menu item
        add_action('admin_menu', function() {
            remove_menu_page('edit.php?post_type=reformed_submission');
        });

        if (is_admin() && isset($_GET['post_type']) && $_GET['post_type'] == 'reformed_submission') {
            $GLOBALS['wp']->add_query_var('post_parent');

            if (current_user_can('edit_posts') && isset($_GET['reformed_export'])) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="submission_export_' . date('YmdHis') . '.csv"');
                header('Cache-Control: no-cache, must-revalidate');

                $handle = fopen('php://output', 'w');
                $submissions = self::get_submissions($_GET['post_parent']);

                if (isset($submissions[0])) {
                    fputcsv($handle, array_keys($submissions[0]));
                }

                foreach ($submissions as $submission) {
                    fputcsv($handle, $submission);
                }

                exit;
            }
        }
    }

    private static function register_post_type() {
        register_post_type('reformed_submission', [
            'label' => 'Submissions',
            'public' => false,
            'show_ui' => true,
            'menu_icon' => 'dashicons-carrot',
            'supports' => [ 'title', 'custom-fields' ],
            'labels' => [
                'singular_name' => 'Submission',
                'add_new_item' => 'Add New Submission',
                'edit_item' => 'Edit Submission',
                'new_item' => 'New Submission',
                'view_item' => 'View Submission',
                'view_items' => 'View Submissions',
                'search_itons' => 'Search Submissions',
                'not_found' => 'No submissions found',
                'not_found_in_trash' => 'No submissions found in Trash',
                'all_items' => 'All Submissions',
                'item_published' => 'Submission published',
            ],
        ]);
    }

    public static function add_meta_box() {
        add_action('add_meta_boxes', function() {
            add_meta_box('reformed-submission-fields', 'Submitted Data', function() {
                foreach (get_post_meta($_GET['post'], 'reformed_form_data', true) as $field => $value) {
                    if (is_array($value)) {
                        $value = implode(', ', $value);
                    }

                    echo "<p><span style='display: block'>$field:</span> <strong>$value</strong></p>";
                }

                if ($submitted_post_id = get_post_meta($_GET['post'], 'submitted_post_id', true)) {
                    echo "<p><span style='display: block'>Submitted via:</span> <strong><a href='" . get_permalink($submitted_post_id) . "'>" . esc_html(get_the_title($submitted_post_id)) . "</a></strong></p>";
                }
            }, 'reformed_submission');
        });
    }

    public static function get_submissions($form_id) {
        global $wpdb; /** @var wpdb $wpdb */

        // use a raw query because WordPress APIs are incredibly slow
        $query = $wpdb->prepare("
            SELECT  meta_value
            FROM    {$wpdb->prefix}postmeta     pm
            JOIN    {$wpdb->prefix}posts        p ON pm.post_id = p.ID
            WHERE   p.post_parent   = %d
            AND     meta_key        = 'reformed_form_data'
        ", $form_id);

        $results        = $wpdb->get_col($query);
        $submissions    = [];

        foreach ($results as $submission) {
            $submissions []= unserialize($submission);
        }

        return $submissions;
    }

}
