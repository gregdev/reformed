<?php

/**
 * Plugin Name: Reformed
 * Version:     1.7.0
 * Author:      Greg Smith
 * Author URI:  https://gregdev.com.au
 * Description: A very work-in-progress form plugin.
 */

add_action('init', 'reformed::init');

foreach (glob(__DIR__ . '/includes/*.php') as $filename) {
    include $filename;
}

foreach (glob(__DIR__ . '/includes/post-types/*.php') as $filename) {
    include $filename;
}

register_deactivation_hook(__FILE__, function () {
    reformed::activate_plugin();
});

class reformed
{
    public static function init()
    {
        wp_register_script('reformed-meta-text-control', plugins_url('blocks/scripts/MetaTextControl.js', __FILE__), [ 'wp-element', 'wp-components', 'wp-edit-post', 'wp-data' ]);
        wp_register_script('reformed-meta-toggle-control', plugins_url('blocks/scripts/MetaToggleControl.js', __FILE__), [ 'wp-element', 'wp-components', 'wp-edit-post', 'wp-data' ]);
        wp_register_script('reformed-action-controls', plugins_url('blocks/scripts/ActionControls.js', __FILE__), [ 'wp-element', 'wp-components', 'wp-edit-post', 'wp-data' ]);

        add_action('wp_enqueue_scripts', function () {
            $version = get_file_data(__FILE__, [ 'Version' => 'Version' ])['Version'];

            wp_enqueue_script('reformed', plugins_url('assets/frontend.js', __FILE__), [ 'jquery' ], $version, true);

            $forms = [];

            foreach (self::get_forms() as $form) {
                $forms[$form->ID] = [
                    'id'                => $form->ID,
                    'clear_on_success'  => (int) get_post_meta($form->ID, 'reformed-form/options/clear-on-success', true),
                    'messages'          => [
                        'success' => esc_attr(get_post_meta($form->ID, 'reformed-form/options/success-message', true)),
                        'failure' => esc_attr(get_post_meta($form->ID, 'reformed-form/options/failure-message', true)),
                    ],
                ];
            }

            wp_localize_script('reformed', 'reformed', [
                'ajax_url'  => admin_url('admin-ajax.php'),
                'forms'     => $forms,
            ]);
        });

        add_action('admin_enqueue_scripts', function () {
            $version = get_file_data(__FILE__, [ 'Version' => 'Version' ])['Version'];

            wp_enqueue_script('reformed', plugins_url('assets/backend.js', __FILE__), [ 'wp-blocks', 'jquery' ], $version, true);
        });
    }

    public static function activate_plugin()
    {
        return; // user roles not yet functional
        global $wp_roles; /** @var WP_Role[]    $wp_roles   */

        $roles = get_editable_roles();

        $capabilities = [
            'read_reformed_forms' ,
            'edit_reformed_forms' ,
            'edit_others_reformed_forms' ,
            'publish_reformed_forms' ,
            'view_submissions_reformed_forms' ,
        ];

        foreach ($wp_roles->role_objects as $key => $role) { /** @var WP_Role $role */
            if (isset($roles[$key]) && $role->has_cap('edit_posts')) {
                foreach ($capabilities as $capability) {
                    $role->add_cap($capability);
                }
            }
        }
    }

    /**
     * @return reformed_form[]
     */
    public static function get_forms()
    {
        $forms = get_posts([
            'post_type'     => 'reformed_form',
            'numberposts'   => -1,
        ]);

        return array_map(function ($post) {
            return new reformed_form($post);
        }, $forms);
    }

    public static function get_form(int $id): reformed_form
    {
        return new reformed_form(get_post($id));
    }
}
